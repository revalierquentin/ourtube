export const videoFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(mp4|MP4)$/)) {
      return callback(new Error('Only video files are allowed!'), false);
    }
    callback(null, true);
  };