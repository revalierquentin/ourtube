import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Records } from './records.entity';
import { Repository, EntityManager } from 'typeorm';
import { User } from '../user/user.entity';

@Injectable()
export class RecordsService {
    @InjectRepository(Records)
    private readonly repository: Repository<Records>;

    public findOne(id: number): Promise<Records> {
        return this.repository.findOneBy({ id });
    }

    public save(record: Records): Promise<Records> {
        return this.repository.save(record);
    }

    public async findAllByUser(user: Express.User): Promise<Records[]> {
        return await this.repository.find({
            relations: {
                video: true
            },
            where: {
                user:user
            }
        });
    }
}
