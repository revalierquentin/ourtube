import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from '../user/user.entity';
import { Video } from '../video/video.entity';
import { ApiProperty, ApiTags } from '@nestjs/swagger';

@Entity()
export class Records extends BaseEntity {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    public id!: number;

    @Column({ type: 'timestamp' })
    @ApiProperty()
    public viewedAt: Date;

    @Column({ type: 'varchar' })
    @ApiProperty()
    public action: string;

    @ManyToOne(type => User, user => user.videos)
    @ApiProperty()
    user: Express.User;

    @ManyToOne(type => Video, video => video.records)
    @ApiProperty()
    video: Video;
}