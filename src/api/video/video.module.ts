import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VideoController } from './video.controller';
import { VideoService } from './video.service';
import { Video } from './video.entity';
import { RecordsModule } from '../records/records.module';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Video]), RecordsModule, UserModule],
  controllers: [VideoController],
  providers: [VideoService]
})
export class VideoModule {}