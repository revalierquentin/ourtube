import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../user/user.entity';
import { Records } from '../records/records.entity';

@Entity()
export class Video extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({ type: 'varchar' })
    public name: string;

    @Column({ type: 'timestamp' })
    public uploadedAt: Date;

    @Column({ type: 'int', default: 0 })
    public views: number;

    @ManyToOne(type => User, user => user.videos)
    user: Express.User;

    @OneToMany(type => Records, records => records.user)
    records: Records[];
}