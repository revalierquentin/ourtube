import { HttpException, HttpStatus, Inject, Injectable, NotFoundException, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { uploadDto } from './video.dto';
import { Video } from './video.entity';
import { Multer } from 'multer';
import { createReadStream, createWriteStream } from 'fs';
import { Request, Response } from 'express';
import { Records } from '../records/records.entity';
import { RecordsService } from '../records/records.service';
import { User } from '../user/user.entity';
import { UserService } from '../user/user.service';

@Injectable()
export class VideoService {
    @InjectRepository(Video)
    private readonly repository: Repository<Video>;
    @Inject(RecordsService)
    private readonly recordsService: RecordsService;
    @Inject(UserService)
    private readonly userService: UserService;

    public async upload(res: Response, req: Request, body: uploadDto, file: Express.Multer.File): Promise<Response> {
        let name = req.body.name;
        let video = new Video();
        video.name = name;
        video.user = req.user;
        video = await this.repository.save(video);
        const writeStream = createWriteStream(`assets/videos/${video.id}.mp4`);
        writeStream.write(file.buffer);
        this.addRecord(video, req.user, "upload")
        return res
            .status(HttpStatus.CREATED)
            .send({ HttpCode: 201, Message: 'Video uploaded' });
    }

    public async download(res: Response, id: number) {
        const video: Video = await this.repository.findOne({ where: { id } });

        if (!video) {
            throw new HttpException('No video found', HttpStatus.NOT_FOUND);
        }

        const filePath = `assets/videos/${id}.mp4`;
        return res.download(filePath, video.name+'.mp4', (err) => {
            if (err) {
                console.log(err);
                return res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                return res.status(HttpStatus.OK).end();
            }
        });
    }

    findOne(id: number): Promise<Video> {
        return this.repository.findOneBy({ id });
    }

    async addView(id: number): Promise<Video> | null {
        let video: Video = await this.repository.findOneBy({ id });
        if(!video){
            return null
        }
        return this.repository.save({ id: id, views: video.views + 1 })
    }

    async addRecord(video: Video, user: Express.User, action: string): Promise<Records> {
        let records = new Records();

        records.video = video;
        records.user = user;
        records.action = action;

        return this.recordsService.save(records);
    }

    public async uploadedVideosByUser(user: Express.User): Promise<Video[]> {
        return await this.repository.find({
            where: {
                user: user
            }
        });
    }

    public async allVideos(): Promise<Video[]> {
        return await this.repository.find();
    }

    public async search(req: Request): Promise<Video[] | Video> {
        // A optimiser
        let res;
        if (req.query.title || req.query.name) {
            let name;
            if (req.query.title) {
                name = req.query.title;
            } else if (req.query.name) {
                name = req.query.name;
            }
            name = name.toString().replace(/["]+/g, '');
            console.log(name);
            res = await this.repository.find({
                relations: {
                    user: true
                },
                select: {
                    name: true,
                    views: true,
                    uploadedAt: true,
                    user: {
                        name: true,
                    }
                },
                where: {
                    name: Like(`%${name}%`)
                }
            });
        } else if (req.query.author || req.query.uploader) {
            let userName;
            if (req.query.author) {
                userName = req.query.author;
            } else if (req.query.uploader) {
                userName = req.query.uploader;
            }
            userName = userName.toString().replace(/["]+/g, '');
            res = await this.repository.find({
                relations: {
                    user: true
                },
                select: {
                    name: true,
                    views: true,
                    uploadedAt: true,
                    user: {
                        name: true,
                    }
                },
                where: {
                    user: await this.userService.findBy(userName)
                }
            });
        } else if ((req.query.title || req.query.name) && (req.query.author || req.query.uploader)) {
            let userName;
            let name;
            if (req.query.title) {
                name = req.query.title;
            } else if (req.query.name) {
                name = req.query.name;
            }
            if (req.query.author) {
                userName = req.query.author;
            } else if (req.query.uploader) {
                userName = req.query.uploader;
            }
            userName = userName.toString().replace(/["]+/g, '');
            name = name.toString().replace(/["]+/g, '');
            res = await this.repository.find({
                relations: {
                    user: true
                },
                select: {
                    name: true,
                    views: true,
                    uploadedAt: true,
                    user: {
                        name: true,
                    }
                },
                where: {
                    name: name,
                    user: await this.userService.findBy(userName)
                }
            });
        }
        return res;
    }
}
