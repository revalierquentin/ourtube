import { ClassSerializerInterceptor, Controller, Req, UseGuards, UseInterceptors, Put, Body, Inject, Post, Get, Param, Header, Res, Headers, HttpStatus, UploadedFile, HttpException, StreamableFile, InternalServerErrorException, ParseFilePipe, MaxFileSizeValidator, FileTypeValidator } from '@nestjs/common';
import { Response } from 'express';
import { JwtAuthGuard } from '@/api/user/auth/auth.guard';
import { uploadDto } from './video.dto';
import { VideoService } from './video.service';
import { Video } from './video.entity';
import { User } from '../user/user.entity';
import { getUser } from '../user/user.decorator';
import { createReadStream, createWriteStream, statSync } from 'fs';
import { FileInterceptor } from '@nestjs/platform-express';
import { Request } from 'express';
import path, { join } from 'path';
import { RecordsService } from '../records/records.service';

@Controller('video')
export class VideoController {
  @Inject(VideoService)
  private readonly service: VideoService;


  @Post('upload')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  uploadVideo(@Res() res: Response, @Req() req: Request, body: uploadDto, @UploadedFile(new ParseFilePipe({
    validators: [
      new MaxFileSizeValidator({ maxSize: 100000000 }),
      new FileTypeValidator({ fileType: 'video/mp4' }),
    ],
  })) file: Express.Multer.File) {
    return this.service.upload(res, req, body, file);
  }

  @Get('download/:id')
  download(@Res() res: Response, @Param('id') id: number) {
    return this.service.download(res, id);
  }

  @Get('stream/:id')
  @UseGuards(JwtAuthGuard)
  @Header('Accept-Ranges', 'bytes')
  @Header('Content-Type', 'video/mp4')
  async getStreamVideo(@Req() req: Request, @Param('id') id: string, @Headers() headers, @Res() res: Response) {
    const videoPath = `assets/videos/${id}.mp4`;
    const { size } = statSync(videoPath);
    const videoRange = headers.range;
    if (videoRange) {
      const parts = videoRange.replace(/bytes=/, "").split("-");
      const start = parseInt(parts[0], 10);
      const end = parts[1] ? parseInt(parts[1], 10) : size - 1;
      const chunksize = (end - start) + 1;
      const readStreamfile = createReadStream(videoPath, { start, end, highWaterMark: 60 });
      const head = {
        'Content-Range': `bytes ${start}-${end}/${size}`,
        'Content-Length': chunksize,
      };
      res.writeHead(HttpStatus.PARTIAL_CONTENT, head); //206
      readStreamfile.pipe(res);
    } else {
      const head = {
        'Content-Length': size,
      };
      let idNum: number = +id;
      let video: Video = await this.service.addView(idNum);
      await this.service.addRecord(video, req.user, "stream");
      res.writeHead(HttpStatus.OK, head);//200
      createReadStream(videoPath).pipe(res);
    }
  }

  @Get('uploaded')
  @UseGuards(JwtAuthGuard)
  async uploadedVideo(@Req() req: Request) {
    return this.service.uploadedVideosByUser(req.user);
  }

  @Get('/')
  async allVideos() {
    return this.service.allVideos();
  }

  @Get('/search')
  getData(@Req() request: Request): Object {
    return this.service.search(request);
  }
}
