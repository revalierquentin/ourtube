import { IsString } from 'class-validator';

export class uploadDto {
  @IsString()
  public readonly name: string;
}