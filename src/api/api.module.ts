import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { VideoModule } from './video/video.module';
import { RecordsModule } from './records/records.module';

@Module({
  imports: [UserModule, VideoModule, RecordsModule]
})
export class ApiModule {}
